package pl.sda.controller;

import pl.sda.model.Car;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

/**
 * Created by Adrian on 22.03.2017.
 */
@ManagedBean(name = "form")
@RequestScoped
public class HomeFormController {
    private String name;
    private String regPlate;
    private String vin;
    private Integer totalMass;

    @ManagedProperty(value = "#{applicationController}")
    private ApplicationController applicationController;

    public ApplicationController getApplicationController() {
        return applicationController;
    }

    public void setApplicationController(ApplicationController applicationController) {
        this.applicationController = applicationController;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRegPlate() {
        return regPlate;
    }

    public void setRegPlate(String regPlate) {
        this.regPlate = regPlate;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public Integer getTotalMass() {
        return totalMass;
    }

    public void setTotalMass(Integer totalMass) {
        this.totalMass = totalMass;
    }

    public void save(){
        this.setName("SET!");

        EntityManager em = applicationController.getEntityManager();

        EntityTransaction transaction = em.getTransaction();

        transaction.begin();
        Car car = new Car();
        car.setMass(this.getTotalMass());
        car.setPlateNumber(this.getRegPlate());
        car.setVin(this.getVin());
        em.persist(car);
        transaction.commit();

        System.out.println("Zapisane do bazy");
    }
}

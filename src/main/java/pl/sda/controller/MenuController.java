package pl.sda.controller;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.persistence.EntityManager;

/**
 * Created by Adrian on 21.03.2017.
 */
@ManagedBean(name = "menuController")
@RequestScoped
public class MenuController {
    /**
     * ta właściwość przeczyta parametr z żądania
     */
    @ManagedProperty(value = "#{param.pageId}")
    private String pageId;

    @ManagedProperty(value = "#{applicationController}")
    private ApplicationController applicationController;

    /**
     * Warunkowa nawigacja pomiędzy stronami
     * @return
     */
    public String showPage() {
        if(pageId == null) {
            return "home";
        }

        if(pageId.equals("1")) {
            return "page1";
        }else if(pageId.equals("2")) {
            return "page2";
        }else {
            return "home";
        }
    }

    public String getPageId() {
        return pageId;
    }

    public void setPageId(String pageId) {
        this.pageId = pageId;
    }

    public ApplicationController getApplicationController() {
        return applicationController;
    }

    public void setApplicationController(ApplicationController applicationController) {
        this.applicationController = applicationController;
    }

    public String getCarList(){
        return "Lista";
    }
}
